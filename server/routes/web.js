const express = require('express');
const { isProduction } = require('../services/srpago/utils');
const router = express.Router();

router.use('/assets', express.static('public'));

router.get('/', (req, res) => {
  return res.render('checkout-form', {
    app_host: process.env.APP_HOST,
    srpago_livemode: isProduction(),
    srpago_apikey: process.env.SRPAGO_APIKEY,
  });
});

module.exports = router;

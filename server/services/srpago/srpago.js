const { Requestor, CipherPayload } = require("./utils");

exports.customer = {
  create: payload => {
    return new Promise(async (resolve, reject) => {
      await Requestor('/v1/customer', true, {
        method: 'POST',
        body: JSON.stringify(payload),
      })
        .then(response => response.json())
        .then(response => {
          if (false === response.success) {
            reject({
              code: response.error.code,
              message: response.error.message,
            });
          } else {
            resolve(response.result);
          }
        });
    });
  },

  addCard: (customer_id, token) => {
    return new Promise(async (resolve, reject) => {
      const payload = {
        token: token,
      };

      await Requestor(`/v1/customer/${customer_id}/cards`, true, {
        method: 'POST',
        body: JSON.stringify(payload),
      })
        .then(response => response.json())
        .then(response => {
          if (false === response.success) {
            reject({
              code: response.error.code,
              message: response.error.message,
            });
          } else {
            resolve(response.result);
          }
        });
    });
  },
};

exports.payment = {
  card: payload => {
    return new Promise(async (resolve, reject) => {
      let chargePayload = {
        metadata: payload.metadata || {}
      };
      delete payload.metadata;

      chargePayload = {
        ...CipherPayload(payload),
        ...chargePayload
      };

      await Requestor('/v1/payment/card', true, {
        method: 'POST',
        body: JSON.stringify(chargePayload),
      })
        .then(response => response.json())
        .then(response => {
          if (false === response.success) {
            reject({
              code: response.error.code,
              message: response.error.message,
            });
          } else {
            resolve(response.result);
          }
        });
    });
  },
};
